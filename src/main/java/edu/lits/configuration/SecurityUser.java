package edu.lits.configuration;

import edu.lits.pojo.Role;
import edu.lits.pojo.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class SecurityUser extends org.springframework.security.core.userdetails.User implements UserDetails {

    private Integer userId;
    private Role role;
    private String name;

    public SecurityUser(User user) {
        super(user.getEmail(), user.getPassword(), Collections.emptyList());
        this.userId = user.getId();
        this.role = user.getRole();
        this.name = user.getName();
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_" + role.getName()));
    }

    public Integer getUserId() {
        return userId;
    }

    public Role getRole() {
        return role;
    }

    public String getName() {
        return name;
    }
}
