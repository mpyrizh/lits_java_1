package edu.lits.controller;

import edu.lits.model.OrderPriceModel;
import edu.lits.pojo.OrderToProduct;
import edu.lits.service.OrderService;
import edu.lits.service.OrderToProductService;
import edu.lits.service.common.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CustomerController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderToProductService orderToProductService;
    
    @RequestMapping(value = "/customer/orders", method = RequestMethod.GET)
    public String orders(Model model) {
        Integer userId = currentUserService.securityUser().getUserId();
        List<OrderPriceModel> orders = orderService.ordersForCustomer(userId);
        model.addAttribute("orders", orders);
        return "/customer/customerOrders";
    }

    @RequestMapping(value = "/customer/orders/{orderId}/products", method = RequestMethod.GET)
    public String customerOrderProducts(@PathVariable Integer orderId, Model model) {
        List<OrderToProduct> orderToProducts = orderToProductService.getByOrderId(orderId);
        model.addAttribute("products", orderToProducts);
        return "/seller/orderProducts";
    }
}
