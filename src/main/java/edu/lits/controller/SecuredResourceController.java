package edu.lits.controller;

import edu.lits.dal.UserDal;
import edu.lits.model.ProductModel;
import edu.lits.pojo.Product;
import edu.lits.service.ProductService;
import edu.lits.service.UserService;
import edu.lits.service.common.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SecuredResourceController {
    @Autowired
    private UserService userService;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserDal userDal;
    @Autowired
    ApplicationContext applicationContext;

    private final ProductService productService;

    public SecuredResourceController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping("/secured")
    public void secureResource(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("accessing secured resource");
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }
    @RequestMapping("/loginIn")
    public String loginIn() {
        return "index";
    }

    @RequestMapping({ "/index", "/" })
    public String index(Model model) {
        List<Product> productList = productService.getProductList();
        List<ProductModel> productModelList = new ArrayList<ProductModel>();

        for (edu.lits.pojo.Product dbProduct : productList) {

            ProductModel productModel = new ProductModel();
            productModel.setId(dbProduct.getId());
            productModel.setName(dbProduct.getName());
            productModel.setDescription(dbProduct.getDescription());
            productModel.setPrice(dbProduct.getPrice());
            productModelList.add(productModel);
        }
        model.addAttribute("productModelList", productModelList);

        return "index";
    }

}