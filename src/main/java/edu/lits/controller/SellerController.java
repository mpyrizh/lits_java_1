package edu.lits.controller;

import edu.lits.model.OrderPriceModel;
import edu.lits.pojo.Order;
import edu.lits.pojo.OrderToProduct;
import edu.lits.service.OrderService;
import edu.lits.service.OrderToProductService;
import edu.lits.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class SellerController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderToProductService orderToProductService;

    @RequestMapping(value = "/seller/users/{id}/orders", method = RequestMethod.GET)
    public String customerOrders(@PathVariable Integer id, Model model) {
        List<OrderPriceModel> ordersforCustomer = orderService.ordersForCustomer(id);
        model.addAttribute("orders", ordersforCustomer);
        model.addAttribute("userId", id);
        return "/seller/customerOrders";
    }

    @RequestMapping(value = "/seller/users/{id}/orders/{orderId}/products", method = RequestMethod.GET)
    public String customerOrderProducts(@PathVariable Integer id, @PathVariable Integer orderId, Model model) {
        List<OrderToProduct> orderToProducts = orderToProductService.getByOrderId(orderId);
        model.addAttribute("products", orderToProducts);
        return "/seller/orderProducts";
    }
}
