package edu.lits.controller;

import edu.lits.dal.UserDal;
import edu.lits.model.UserModel;
import edu.lits.pojo.User;
import edu.lits.service.OrderService;
import edu.lits.service.ProductService;
import edu.lits.service.UserService;
import edu.lits.service.common.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping (value="/user")
@Controller
public class SellerInfoController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private ProductService productService;

    @Autowired
	private UserDal userDal;

    @Autowired(required = true)
    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    List<UserModel> userModelList = new ArrayList<>();
    UserModel userModel = new UserModel();

    @RequestMapping(value = "/seller/info",
            method = RequestMethod.GET)
    public String getSellerInfo(Model model) {
//
        final Integer userId = currentUserService.securityUser().getUserId();
        User user = userDal.read(userId);
        model.addAttribute("userModel",user);


        return "getSellerInfo";
    }


    @GetMapping( "/seller/info/edit/{id}")
    public String editSellerInfo(@PathVariable Integer id, Model model) {
        User user = userService.read(id);
        model.addAttribute("userModel", user);

        return "editSellerInfo";
    }
    @ResponseBody
    @RequestMapping(value = "/users/seller/{id}/image", method = RequestMethod.GET)
    public ResponseEntity<Resource> getImage(@PathVariable Integer id) {
        Resource resource = userService.loadImage(id);
        String fileName = resource == null ? null : resource.getFilename();
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + fileName + "\"").body(resource);
    }
    @PostMapping("/seller/info/save/image/{id}")
    public String updateUserImageProfile(@PathVariable Integer id, @Validated @ModelAttribute UserModel userModel, BindingResult bindingResult) {
        // code to update user
        User user1 = userService.read(id);
       /* user1.setName(userModel.getName());*/
        /*userService.saveOrUpdate(user1);*/
        userService.saveImage(userModel, user1);
        return "redirect:/Seller/info";
    }

    @PutMapping( value = "/seller/info/save/{id}")
    public String updateUserProfile(@PathVariable Integer id, @Validated @ModelAttribute UserModel userModel, BindingResult bindingResult) {
        // code to update user
        User user1 = userService.read(id);
        user1.setName(userModel.getName());
        userService.saveOrUpdate(user1);
        /*userService.saveImage(user, user1);*/
        return "redirect:/user/seller/info";
    }
   /* @RequestMapping(value = "/seller/save",
            method = RequestMethod.POST)
    public String saveSellerInfo(Model model, @ModelAttribute("userModel") @Validated UserModel userModel, final RedirectAttributes redirectAttributes) {

        Integer userId = currentUserService.securityUser().getUserId();
        User user = userDal.read(userId);

//        user.setId(1);
//        user.setName(userModel.getName());
        userService.save(userModel);
        model.addAttribute("userModel",userModel);

        return "getSellerInfo";
    }*/

    @RequestMapping(value = "/leftMenu",
            method = RequestMethod.GET)
    public String leftMenu (Model model) {

        return "leftMenu";
    }


    @RequestMapping(value = "/list",
            method = RequestMethod.GET)
    public String userList(Model model) {

        List<User> list= userService.findAllCustomers();
        List<UserModel> userModelList = new ArrayList<UserModel>();

        for (edu.lits.pojo.User dbProduct : list) {

            UserModel userModel = new UserModel();
            userModel.setId(dbProduct.getId());
            userModel.setName(dbProduct.getName());
//			userModel.setPhoneNumber(dbProduct.getNameRole ());

            userModelList.add(userModel);

        }
        System.out.println(userModelList);
        model.addAttribute("userList", userModelList);
        return "getAllUser";
    }


}