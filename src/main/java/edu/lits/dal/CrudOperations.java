package edu.lits.dal;

import java.util.List;

public interface CrudOperations<T> {
    /**
     * Get pojo by id
     *
     * @param id pojo id
     * @return pojo
     */
    T read(Integer id);

    List<T> readAll();

    T saveOrUpdate(T entity);

    void delete(T entity);
}
