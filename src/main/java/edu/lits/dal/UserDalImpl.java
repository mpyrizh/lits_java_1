package edu.lits.dal;

import edu.lits.pojo.User;
import edu.lits.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDalImpl extends CrudOperationsImpl<User> implements UserDal {

    private SessionFactory sessionFactory;
    private final UserService userService;

    public UserDalImpl(SessionFactory sessionFactory, UserService userService) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
        this.userService = userService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> userList() {

        Session session = this.sessionFactory.openSession();
        List<User> userList = session.createQuery("from User").list();
        session.close();
        System.out.println(userList);
        return userList;

    }

  /*  @Override
    public User getCurrentUser(Integer userId) {
        return null;
    }*/

    @Override
    public User saveOrUpdate(User user) {

        return super.saveOrUpdate(user);
    }

    @Override
    public User read(Integer id) {
        return super.read(id);
    }

    @Override
    public void delete(User user) {
        super.delete(user);
    }
/*
    @Override
    public User addUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
        session.close();
        return null;
    }

    @Override
    public User getUserById(int id) {
        Session session =this.sessionFactory.getCurrentSession();
        User user = (User) session.load(User.class, new Integer(id));
        session.close();
        return user;
    }

    @Override
    public User updateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
        session.close();
        return user;
    }

    @Override
    public User removeUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = (User) session.load(User.class, new Integer(id));

        if (user != null) {
            session.delete(user);
        }
        session.close();
        return user;

    }*/

    @Override
    public User getByEmail(String email) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = (User) session.createQuery("select u from User u where u.email = :email")
                .setParameter("email", email)
                .uniqueResult();
        tx.commit();
        session.close();
        return user;
    }

//    select u.id, u.email, r.name, count(o.seller_id) from user u
//    join roles r on u.role_id = r.id
//    join orders o on o.seller_id = u.id
//    where r.name = 'SELLER'
//    group by o.seller_id
//    order by count(o.seller_id)
//    limit 1

    @Override
    public User findByMinOrders() {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("select u from User u " +
                "join u.role r " +
                "left join u.sellerOrders o " +
                "where r.name = 'SELLER' " +
                "group by o.seller " +
                "order by count(o.seller)");
        query.setMaxResults(1);
        User user = (User) query.list().get(0);
        tx.commit();
        session.close();
        return user;
    }

    @Override
    public List<User> findAllCustomers() {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        final Query query = session.createQuery("select u from User u " +
                "join u.role r " +
                "where r.name = 'CUSTOMER'");
        return query.list();
    }
}
