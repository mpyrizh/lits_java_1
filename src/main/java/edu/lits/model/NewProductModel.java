package edu.lits.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

public class NewProductModel {

    private Integer productId;

  @Min(value = 0L,message = "The value must be positive")
    private Integer count = 1;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
