package edu.lits.model;

import edu.lits.validation.UniqueEmail;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.BlackList;
import javax.validation.constraints.Pattern;

public class UserModel {
	private Integer id;
	private String name;
	private String phoneNumber;
	private String nameRole;
	private MultipartFile image;

	@UniqueEmail(payload = {BlackList.class})
	@NotBlank(message = "Email must not be empty.")
	@Email(message = "Email is not valid.")
	private String email;

	@Pattern(regexp = ".*[0-9]+", message = "Enter at least one digit")
	@NotBlank(message = "Password must not be null.")
	private String password;

	public String getNameRole() {
		return nameRole;
	}

	public void setNameRole(String nameRole) {
		this.nameRole = nameRole;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}
}
