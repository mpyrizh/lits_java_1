package edu.lits.pojo;

public enum OrderStatus {

    PREPARE,
    DONE,
    PENDING
}
