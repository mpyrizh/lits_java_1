package edu.lits.service;

import java.util.Map;

public interface EmailService {

    void sendSimpleMessage(String to, String subject, String text);

    void send(String to, String subject, String templateName, Map<String, Object> data);
}
