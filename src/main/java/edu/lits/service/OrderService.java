package edu.lits.service;

import edu.lits.model.NewProductModel;
import edu.lits.model.OrderModel;
import edu.lits.model.OrderPriceModel;
import edu.lits.model.ProductInOrderModel;
import edu.lits.model.ProductModel;
import edu.lits.pojo.Order;

import java.util.List;

public interface OrderService {
    List<Order> getOrderList();

    Order getCurrent(Integer userId);

    void addToCurrentOrder(Integer userId, NewProductModel newProductModel);

    List<ProductInOrderModel> getCurrentOrderProducts(Integer userId);

    void deleteFromCurrentOrder(Integer userId, NewProductModel newProductModel);

    void editCurrentOrder(Integer userId, NewProductModel newProductModel);

    Order buy(Integer idOfUser);

    List<OrderPriceModel> ordersForCustomer(Integer id);

    Order read(Integer orderId);
}
