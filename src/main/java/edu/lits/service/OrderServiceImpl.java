package edu.lits.service;

import edu.lits.dal.OrderDal;
import edu.lits.dal.UserDal;
import edu.lits.model.NewProductModel;
import edu.lits.model.OrderModel;
import edu.lits.model.OrderPriceModel;
import edu.lits.model.ProductInOrderModel;
import edu.lits.pojo.Order;
import edu.lits.pojo.OrderStatus;
import edu.lits.pojo.OrderToProduct;
import edu.lits.pojo.Product;
import edu.lits.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDal orderDal;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderToProductService orderToProductService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserDal userDal;

    public void setOrderDal(OrderDal orderDal) {
        this.orderDal = orderDal;
    }

    public OrderDal getOrderDal() {
        return orderDal;
    }

    @Override
    public List<Order> getOrderList() {
        List<edu.lits.pojo.Order> list = orderDal.readAll();
        return list;
    }

    @Override
    public Order getCurrent(Integer userId) {
        return orderDal.getCurrentOrder(userId);
    }

    @Override
    public void addToCurrentOrder(Integer userId, NewProductModel newProductModel) {
        Order currentOrder = orderDal.getCurrentOrder(userId);
        Product product = productService.read(newProductModel.getProductId());
        OrderToProduct orderToProduct = null;
        if (currentOrder == null) {
            User currentUser = userService.read(userId);
            currentOrder = new Order();
            currentOrder.setStatus(OrderStatus.PREPARE);
            currentOrder.setCustomer(currentUser);
            currentOrder.setCreatedAt(LocalDateTime.now());


        } else {
            orderToProduct = orderToProductService.getByOrderAndProduct(currentOrder, product);
        }

        if (orderToProduct == null) {
            orderToProduct = new OrderToProduct();
        }
        orderDal.saveOrUpdate(currentOrder);

        orderToProduct.setOrder(currentOrder);
        orderToProduct.setProduct(product);
        Integer count = newProductModel.getCount();
        if (orderToProduct.getProductCount() != null) {
            count += orderToProduct.getProductCount();
        }
        orderToProduct.setProductCount(count);
        orderToProductService.saveOrUpdate(orderToProduct);
    }

    @Override
    public List<ProductInOrderModel> getCurrentOrderProducts(Integer userId) {
        List<ProductInOrderModel> result;
        Order currentOrder = orderDal.getCurrentOrder(userId);
        if (currentOrder == null) {
            result = new ArrayList<>();
        } else {
            result = orderDal.getCurrentOrderProducts(currentOrder);
        }
        return result;
    }

    @Override
    public void deleteFromCurrentOrder(Integer userId, NewProductModel newProductModel) {
        Order currentOrder = orderDal.getCurrentOrder(userId);
        Product product = productService.read(newProductModel.getProductId());

        OrderToProduct orderToProduct = orderToProductService.getByOrderAndProduct(currentOrder, product);


        orderToProductService.delete(orderToProduct);
    }
    @Override
    public void editCurrentOrder(Integer userId, NewProductModel newProductModel) {
        Order currentOrder = orderDal.getCurrentOrder(userId);
        Product product = productService.read(newProductModel.getProductId());

        OrderToProduct orderToProduct = orderToProductService.getByOrderAndProduct(currentOrder, product);

        Integer count = newProductModel.getCount();

        orderToProduct.setProductCount(count);

        orderToProductService.saveOrUpdate(orderToProduct);
    }

    @Override
    public Order buy(Integer userId) {
        List<ProductInOrderModel> products = getCurrentOrderProducts(userId);
        double total = 0;
        for (ProductInOrderModel product : products) {
            total = total + product.getPrice() * product.getCount();
        }

        User seller = userDal.findByMinOrders();
        Order currentOrder = orderDal.getCurrentOrder(userId);
        currentOrder.setStatus(OrderStatus.PENDING);
        currentOrder.setSeller(seller);
        currentOrder.setBoughtAt(LocalDateTime.now());
        orderDal.saveOrUpdate(currentOrder);

        User user = userService.read(userId);
        LocalDateTime data = currentOrder.getBoughtAt();
        Map<String, Object> dataForCustomer = new HashMap<>();
        dataForCustomer.put("name", user.getName());
        dataForCustomer.put("products", products);
        dataForCustomer.put("total", total);
        dataForCustomer.put("seller", seller);
        dataForCustomer.put("boughtAt", data);

        try {
            emailService.send(user.getEmail(), "buy", "email/infoForCustomer", dataForCustomer);
            emailService.send(seller.getEmail(), "buy", "email/infoForSeller", dataForCustomer);
        } catch (Exception e) {
        }
        return currentOrder;
    }

    @Override
    public List<OrderPriceModel> ordersForCustomer(Integer id) {
        List<OrderPriceModel> result = new ArrayList<>();
        List<Order> orders = orderDal.ordersForCustomer(id);

        for (Order order : orders) {
            OrderModel orderModel = new OrderModel();
            orderModel.setId(order.getId());
            orderModel.setStatus(order.getStatus());
            List<OrderToProduct> orderToProducts = order.getProducts();
            double total = calculateTotal(orderToProducts);
            OrderPriceModel orderPriceModel = new OrderPriceModel(orderModel, total);
            result.add(orderPriceModel);
        }
        return result;
    }

    @Override
    public Order read(Integer orderId) {

        return orderDal.read(orderId);
    }

    private double calculateTotal(List<OrderToProduct> products) {
        double total = 0;
        for (OrderToProduct orderToProduct : products) {
            Integer price = orderToProduct.getProduct().getPrice() == null ? 0 : orderToProduct.getProduct().getPrice();
            total = total + price * orderToProduct.getProductCount();
        }
        return total;
    }
}
