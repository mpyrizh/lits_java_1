package edu.lits.service;

import edu.lits.model.ProductInOrderModel;
import edu.lits.model.ProductModel;
import edu.lits.pojo.Product;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ProductService {

    public List<Product> getProductList();

    Product read(Integer productId);
    Product create(ProductModel productModel);
    Product edit(ProductModel productModel,Integer productId);
    Product delete(Integer productId);
    Product deleteImage(Integer productId);

    Resource loadImage(Integer productId);

    double calculateTotal(List<ProductInOrderModel> products);
}
