package edu.lits.service;

import edu.lits.dal.ProductDal;
import edu.lits.model.ProductInOrderModel;
import edu.lits.model.ProductModel;
import edu.lits.pojo.Product;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDal productDal;

    @Autowired
    private StorageService storageService;

    public void setProductDal(ProductDal productDal) {
        this.productDal = productDal;
    }

    public ProductDal getProductDal() {
        return productDal;
    }

    @Override
    public List<Product> getProductList() {
        List<edu.lits.pojo.Product> list = productDal.readAll();
        return list;
    }

    @Override
    public Product read(Integer productId) {
        return productDal.read(productId);
    }

    @Override
    public Product create(ProductModel productModel) {
        Product newProduct = new Product();
        newProduct.setName(productModel.getName());
        newProduct.setDescription(productModel.getDescription());
        newProduct.setPrice(productModel.getPrice());
        newProduct.setQuantity(productModel.getQuantity());
        saveImage(productModel, newProduct);
        return newProduct;
    }

    @Override
    public Product edit(ProductModel productModel, Integer productId) {
        Product editedProduct = read(productId);
        editedProduct.setName(productModel.getName());
        saveImage(productModel, editedProduct);
        productDal.saveOrUpdate(editedProduct);
        return editedProduct;
    }

    @Override
    public Resource loadImage(Integer productId) {
        Resource result = null;
        Product product = read(productId);
        if (product.getImageExtension() != null) {
            String fileSeparator = System.getProperty("file.separator");
            String imageLocation = fileSeparator + "products" + fileSeparator + productId + "." + product.getImageExtension();
            result = storageService.load(imageLocation);
        }
        return result;
    }

    @Override
    public Product delete(Integer productId) {
        Product deletedProduct = read(productId);
        productDal.delete(deletedProduct);
        if (deletedProduct.getImageExtension() != null) {
            String imageLocation = getProductImagePath(productId, deletedProduct.getImageExtension());
            storageService.delete(imageLocation);
        }
        return deletedProduct;
    }

    @Override
    public Product deleteImage(Integer productId) {
        Product product = read(productId);
        String imageLocation = getProductImagePath(productId, product.getImageExtension());
        storageService.delete(imageLocation);
        product.setImageExtension(null);
        productDal.saveOrUpdate(product);
        return product;
    }

    private void saveImage(ProductModel productModel, Product editedProduct) {
        if (productModel.getImage().getSize() > 0) {
            String extension = FilenameUtils.getExtension(productModel.getImage().getOriginalFilename());
            editedProduct.setImageExtension(extension);
        }
        productDal.saveOrUpdate(editedProduct);
        if (productModel.getImage().getSize() > 0) {
            String fileSeparator = System.getProperty("file.separator");
            String imageLocation = fileSeparator + "products" + fileSeparator + editedProduct.getId();
            storageService.store(productModel.getImage(), imageLocation);
        }
    }

    @Override
    public double calculateTotal(List<ProductInOrderModel> products) {
        double total = 0;
        for (ProductInOrderModel product : products) {
            Integer price = product.getPrice() == null ? 0 : product.getPrice();
            total = total + price * product.getCount();
        }
        return total;
    }

    private String getProductImagePath(Integer productId, String extension) {
        String fileSeparator = System.getProperty("file.separator");
        return fileSeparator + "products" + fileSeparator + productId + "." + extension;
    }
}
