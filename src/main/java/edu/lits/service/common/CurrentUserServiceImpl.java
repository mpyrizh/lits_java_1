package edu.lits.service.common;

import edu.lits.configuration.SecurityUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserServiceImpl implements CurrentUserService {

    public SecurityUser securityUser() {
        SecurityUser result;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal().equals("anonymousUser")) {
            result = null;
        } else {
            result = ((SecurityUser) authentication.getPrincipal());
        }
        return result;
    }
}
