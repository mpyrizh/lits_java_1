package lesson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MainJDBC {

	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL
			= "jdbc:mysql://sql7.freemysqlhosting.net/sql7263942";

	static final String USER = "sql7263942";
	static final String PASS = "kDwHXU2EQ2";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;

		List<Group> groupList = new ArrayList<>();
		try {

			Class.forName(JDBC_DRIVER);


			System.out.println("Connecting to database...");
			conn = DriverManager
					.getConnection(DB_URL, USER, PASS);


			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT id, name, description " +
					"FROM sql7263942.group ";
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				Group group = new Group();
				//Retrieve by column name
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String description = rs.getString("description");
				group.setId(id);
				group.setName(name);
				group.setDescription(description);
				groupList.add(group);

				System.out.print("ID: " + id);
				System.out.print(", name: " + name);
				System.out.print(", description: " + description);
			}

			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {

			se.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Goodbye!");

		groupList.stream().forEach(x -> System.out.println(x.getName()));
	}

	public List<Group> mainServletJDBC() {
		Connection conn = null;
		Statement stmt = null;

		List<Group> groupList = new ArrayList<>();
		try {

			Class.forName(JDBC_DRIVER);


			System.out.println("Connecting to database...");
			conn = DriverManager
					.getConnection(DB_URL, USER, PASS);


			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT id, name, description " +
					"FROM sql7263942.group ";
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				Group group = new Group();
				//Retrieve by column name
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String description = rs.getString("description");
				group.setId(id);
				group.setName(name);
				group.setDescription(description);
				groupList.add(group);

				System.out.print("ID: " + id);
				System.out.print(", name: " + name);
				System.out.print(", description: " + description);
			}

			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {

			se.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Goodbye!");

		groupList.stream().forEach(x -> System.out.println(x.getName()));
		return groupList;
	}

}
