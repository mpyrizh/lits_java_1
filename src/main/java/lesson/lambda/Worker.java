package lesson.lambda;

public class Worker {

	final static String salutation = "Hello ";

	public static void main(String args[]) {
		int i = 1;
		GreetingService greetService1 = a -> salutation + a;


		String resultValue = greetService1.sayMessage("world");
	}

	interface GreetingService {
		String sayMessage(String message);
	}
}