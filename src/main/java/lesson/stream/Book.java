package lesson.stream;

public class Book {

	private String fieldS;
	private Double fieldD;
	private Integer fieldI;

	public Book(String fieldS, Double fieldD, Integer fieldI) {
		this.fieldS = fieldS;
		this.fieldD = fieldD;
		this.fieldI = fieldI;
	}

	public String getFieldS() {
		return fieldS;
	}

	public void setFieldS(String fieldS) {
		this.fieldS = fieldS;
	}

	public Double getFieldD() {
		return fieldD;
	}

	public void setFieldD(Double fieldD) {
		this.fieldD = fieldD;
	}

	public Integer getFieldI() {
		return fieldI;
	}

	public void setFieldI(Integer fieldI) {
		this.fieldI = fieldI;
	}

	@Override
	public void finalize() {
		System.out.println("book going to be cleaned");
	}
}
